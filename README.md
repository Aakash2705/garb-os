# THE GARB OPERATING SYSTEM FOR X86 SYSTEMS

## Build Instructions
### Dependencies:
gcc nasm mtools make dd
bochs (optional)

After installing the required dependencies, run 
    `make`

You should have the floppy image in the `build` dir
