ASM=nasm
CC=gcc

SRCDIR=src
BUILDIR=build

.PHONY: all floppy_image tools_fat kernel bootloader clean always

all: tools_fat floppy_image

floppy_image: $(BUILDIR)/garb.img

$(BUILDIR)/garb.img : bootloader kernel always
	dd if=/dev/zero of=$(BUILDIR)/garb.img bs=512 count=2880
	mkfs.fat -F 12 -n "GARB" $(BUILDIR)/garb.img
	dd if=$(BUILDIR)/boot.bin of=$(BUILDIR)/garb.img conv=notrunc
	mcopy -i $(BUILDIR)/garb.img $(BUILDIR)/kernel.bin "::kernel.bin"
	mcopy -i $(BUILDIR)/garb.img test.txt "::test.txt"

bootloader: $(BUILDIR)/boot.bin

$(BUILDIR)/boot.bin: $(SRCDIR)/boot/boot.asm always
	$(ASM) -f bin $(SRCDIR)/boot/boot.asm -o $(BUILDIR)/boot.bin

kernel: $(BUILDIR)/kernel.bin

$(BUILDIR)/kernel.bin: $(SRCDIR)/kernel/kernel.asm always
	nasm -f bin $(SRCDIR)/kernel/kernel.asm -o $(BUILDIR)/kernel.bin

tools_fat: $(BUILDIR)/tools/fat
$(BUILDIR)/tools/fat: always $(SRCDIR)/tools/fat/fat.c
	mkdir -p $(BUILDIR)/tools
	$(CC) -g -o $(BUILDIR)/tools/fat $(SRCDIR)/tools/fat/fat.c

clean:
	rm -rf $(BUILDIR)/*

always:
	mkdir -p $(BUILDIR)

bochs:
	bochs -qf bochs_config
