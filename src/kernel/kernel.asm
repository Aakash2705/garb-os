[org 0x0]
[bits 16]

%include "src/kernel/gdt.asm"

jmp main
main:
  mov si, msg_hello
  call puts

  ; loading the GDT and switching to 32 bit protected mode
  cli
  lgdt [gdt_descriptor]

  mov eax, cr0
  or eax, 0x1
  mov cr0, eax

  jmp CODE_SEG:init_pm
  

  hlt

; si points to string to print
puts:
  push si
  push ax
.loop:
  lodsb
  or al, al
  je .done

  mov ah, 0x0e
  mov bh, 0
  int 0x10

  jmp .loop
.done:
  pop ax
  pop si
  ret

[bits 32]
init_pm:
  mov ax, DATA_SEG
  mov ds, ax
  mov ss, ax
  mov es, ax
  mov fs, ax
  mov gs, ax

  mov ebp, 0x90000
  mov esp, ebp

  jmp begin_pm

begin_pm:
  mov ebx, MSG_PROT_MODE
  call puts_pm

  jmp $

puts_pm:
  pusha
  mov edx, VID_MEM
.puts_pm_loop:
  mov al, [ebx]
  mov ah, WHITE_ON_BLACK

  cmp al, 0
  je .puts_pm_done

  mov [edx], ax

  add ebx, 1
  add edx, 2

  jmp .puts_pm_loop

.puts_pm_done:
  popa
  ret

VID_MEM equ 0xB8000
WHITE_ON_BLACK equ 0x0f

msg_hello: db "Hello, World!", 0x0A, 0
MSG_PROT_MODE: db "OS IS NOW IN 32 BIT PROTECTED MODE"
